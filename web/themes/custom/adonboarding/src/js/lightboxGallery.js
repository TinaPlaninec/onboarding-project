(function ($, Drupal) {
  Drupal.behaviors.lightboxGallery = {
    attach: function (context, settings) {
      once("galleryInit", ".gallery-wrapper", context).forEach((el) => {
        // Handle the min height of slider to set it on resize and when the lightbox opens/closes
        const slider = el.querySelector(".gallery-slider");
        const sliderTop = el.querySelector(".gallery-slider-top");
        const sliderBottom = el.querySelector(".gallery-slider-bottom");

        const setSliderMinHeight = () => {
          const sliderBottomMargin = getComputedStyle(sliderBottom).marginTop;
          if (slider.closest(".modal").classList.contains("is-open")) {
            const sliderHeight =
              sliderTop.getBoundingClientRect().height +
              sliderBottom.getBoundingClientRect().height +
              parseInt(sliderBottomMargin, 10);

            slider.style.minHeight = `${Math.ceil(sliderHeight)}px`;
          } else {
            slider.style.minHeight = "unset";
          }
        };

        /* -- Modal/Lightbox --*/
        let focusedElBeforeOpen;
        const body = document.querySelector("body");
        const main = document.querySelector(".main");
        const modal = el.querySelector(".modal");
        const sliderBtnPrev = el.querySelector(".btn-prev");
        const sliderBtnNext = el.querySelector(".btn-next");

        const openGalleryModal = (e) => {
          e.preventDefault();
          focusedElBeforeOpen = document.activeElement;
          body.classList.add("modal-open");
          modal.setAttribute("aria-hidden", false);
          modal.classList.remove("is-hidden");
          modal.classList.add("is-open");
          main.setAttribute("aria-hidden", true);
          modal.focus();
          document.addEventListener("keydown", handleKeyDown);

          setTimeout(() => {
            setSliderMinHeight();
          }, "100");
        };

        const closeGalleryModal = () => {
          body.classList.remove("modal-open");
          modal.setAttribute("aria-hidden", true);
          modal.classList.remove("is-open");
          modal.classList.add("is-hidden");
          main.setAttribute("aria-hidden", false);
          focusedElBeforeOpen.focus();
          document.removeEventListener("keydown", handleKeyDown);
          sliderBtnNext.setAttribute("aria-disabled", false);
          sliderBtnPrev.setAttribute("aria-disabled", false);
          setSliderMinHeight();
        };

        //Get all elements inside modal that are focusable
        const getFocusableChildren = () => {
          const focusableElements = modal.querySelectorAll(
            'button:not([disabled]), [tabindex="0"]'
          );
          return [...focusableElements];
        };

        // Once the modal is open, keep focus in it while tabbing
        const trapTabKeyInModal = (e) => {
          const focusableChildren = getFocusableChildren();
          const focusedItemIndex = focusableChildren.indexOf(
            document.activeElement
          );
          const lastIndex = focusableChildren.length - 1;
          const withShift = e.shiftKey;

          // Handle backward (SHIFT and TAB keys) and forward (TAB key alone) tabbing
          if (withShift && focusedItemIndex === 0) {
            focusableChildren[lastIndex].focus();
            e.preventDefault();
          } else if (!withShift && focusedItemIndex === lastIndex) {
            focusableChildren[0].focus();
            e.preventDefault();
          }
        };

        const handleKeyDown = (e) => {
          if (e.key === "Escape") {
            closeGalleryModal();
          } else if (e.key === "Tab") {
            trapTabKeyInModal(e);
          }
        };

        // Handle Close modal button click
        const closeModalBtn = el.querySelector(".close-modal");

        if (closeModalBtn) {
          closeModalBtn.addEventListener("click", closeGalleryModal);
        }

        /*-- Slider --*/
        const handleDisabledButton = (slideNumber, slideElements) => {
          let slideElementsNum = slideElements.length;

          const disableButton = (type) => {
            switch (type) {
              case "Both":
                sliderBtnNext.classList.add("disabled");
                sliderBtnNext.setAttribute("aria-disabled", true);
                sliderBtnPrev.classList.add("disabled");
                sliderBtnPrev.setAttribute("aria-disabled", true);
                break;
              case "Prev":
                sliderBtnPrev.classList.add("disabled");
                sliderBtnPrev.setAttribute("aria-disabled", true);
                if (sliderBtnNext.classList.contains("disabled")) {
                  sliderBtnNext.classList.remove("disabled");
                  sliderBtnNext.setAttribute("aria-disabled", false);
                }
                break;
              case "Next":
                sliderBtnNext.classList.add("disabled");
                sliderBtnNext.setAttribute("aria-disabled", true);
                if (sliderBtnPrev.classList.contains("disabled")) {
                  sliderBtnPrev.classList.remove("disabled");
                  sliderBtnPrev.setAttribute("aria-disabled", false);
                }
                break;
              default:
                sliderBtnNext.classList.remove("disabled");
                sliderBtnNext.setAttribute("aria-disabled", false);
                sliderBtnPrev.classList.remove("disabled");
                sliderBtnPrev.setAttribute("aria-disabled", false);
            }
          };

          if (slideElementsNum > 1 && slideNumber >= slideElementsNum) {
            disableButton("Next");
          } else if (slideElementsNum > 1 && slideNumber <= 1) {
            disableButton("Prev");
          } else if (
            (slideElementsNum =
              1 && (slideNumber >= slideElementsNum || slideNumber <= 1))
          ) {
            disableButton("Both");
          } else {
            disableButton();
          }
        };

        // Set slide index so that the slider stops when you're on the first or last slide
        const loopIndex = (slideNumber, slideElements, slideIndex) => {
          if (slideNumber > slideElements.length) {
            slideIndex = slideElements.length;
          } else if (slideNumber < 1) {
            slideIndex = 1;
          }
          return slideIndex;
        };

        let currentSlideIndex = 1;
        const galleryThumbnails = el.querySelectorAll(
          ".gallery-slider-thumbnail"
        );

        // Handle setting is-active class and aria-hidden attribute
        const handleIsActive = (arr, setAriaHidden, i = currentSlideIndex) => {
          arr.forEach((el) => {
            if (setAriaHidden) {
              el.setAttribute("aria-hidden", "true");
            }
            el.classList = el.className.replace(" is-active", "");
          });
          arr[i - 1].className += " is-active";
          if (setAriaHidden) {
            arr[i - 1].setAttribute("aria-hidden", "false");
          }
        };

        const showSlides = (slideNum) => {
          const gallerySlides = el.querySelectorAll(".gallery-slide");

          //Set slide index
          currentSlideIndex = loopIndex(
            slideNum,
            gallerySlides,
            currentSlideIndex
          );

          // Set disabled buttons and add 'is-active' class to current slide and its thumbnail
          handleDisabledButton(currentSlideIndex, gallerySlides);
          handleIsActive(gallerySlides, true);
          handleIsActive(galleryThumbnails, false);
        };

        const currentSlide = (slideNum) => {
          showSlides((currentSlideIndex = slideNum));
        };

        // Open lightbox gallery on thumbnail click and set/show the correct image slide
        const imageThumbnails = el.querySelectorAll(
          ".gallery-thumbnail.open-modal a"
        );

        if (imageThumbnails.length) {
          imageThumbnails.forEach((thumbnail) => {
            const thumbnailNum = thumbnail.parentNode.dataset.thumbnail;
            thumbnail.addEventListener("click", function (e) {
              openGalleryModal(e);
              currentSlide(+thumbnailNum);
            });
          });
        }

        // On slider gallery thumbnail click, set/show the matching slide
        if (galleryThumbnails.length) {
          galleryThumbnails.forEach((thumbnail) => {
            const thumbnailNum = thumbnail.dataset.sliderThumbnail;

            thumbnail.addEventListener("click", () => {
              currentSlide(+thumbnailNum);
            });
          });
        }

        // Handle slider prev, next slide buttons
        const changeSlides = (n) => {
          showSlides((currentSlideIndex += n));
        };

        ["click", "keydown"].forEach((ev) => {
          sliderBtnPrev.addEventListener(ev, function (e) {
            if (
              ev == "click" ||
              e.key === "Enter" ||
              e.key === " " ||
              (e.key === "Control" && e.key === "Alt" && e.key === " ")
            ) {
              e.preventDefault();
              changeSlides(-1);
            }
          });

          sliderBtnNext.addEventListener(ev, function (e) {
            if (
              ev == "click" ||
              e.key === "Enter" ||
              e.key === " " ||
              (e.key === "Control" && e.key === "Alt" && e.key === " ")
            ) {
              e.preventDefault();
              changeSlides(1);
            }
          });
        });

        //Set slider min-height on resize
        window.addEventListener("resize", () => {
          if (modal.classList.contains("is-open")) {
            setSliderMinHeight();
          }
        });
      });
    },
  };
})(jQuery, Drupal);
