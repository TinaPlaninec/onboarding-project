# adonboarding THEME INFORMATION
-------------

## Local environment
   - Node.js: v14.17.4
   - Npm: v6.14.14

## Workflow
   run `npm install` to install all necessary packages
   run `npm run dev` or `npm run prod` script

## Available Scripts
   - `npm run dev`
     it is used for development in local environment. It bundles all .js and
     .scss files located in src folder into all.js and styles.css file located
     in dist folder. It also creates source map file and watches for any changes.

   - `npm run prod`
     it is used for production build. It bundles all .js and .scss files located in
     src folder into all.js and styles.css file located in dist folder. It also
     minifies the output.

