(function ($, Drupal) {
  Drupal.behaviors.infiniteScrollAnimation = {
    attach: function (context, settings) {
      const $eventViewElement = $(
        ".view-events div.views-infinite-scroll-content-wrapper",
        context
      );

      $(context).ajaxSuccess(function (event, xhr, settings) {
        if (
          settings.url.indexOf("/views/ajax") != -1 &&
          settings.data.startsWith("view_name=events")
        ) {
          Drupal.behaviors.infiniteScrollAnimation.initAnimation(
            $eventViewElement
          );
        }
      });
    },
    initAnimation: function ($selector) {
      const $eventView = $selector;
      const $viewRows = $eventView.children();
      const $previouslyAddedRows = $viewRows.slice(0, -6);

      $eventView
        .find(".views-row")
        .once("rowInit")
        .each(function () {
          if ($viewRows.length > 6) {
            for (let i = 0; i < $previouslyAddedRows.length; i++) {
              let element = $previouslyAddedRows[i];
              if ($(element).hasClass("animate")) {
                $(element).removeClass("animate");
              }
            }
            $(this).addClass("animate");
          }
        });
    },
  };
})(jQuery, Drupal);

//# sourceMappingURL=infiniteScrollAnimation.js.map
