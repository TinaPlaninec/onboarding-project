(function ($, Drupal) {
  $(window).on("scroll", function () {
    var $scrollBarLocation = $(this).scrollTop();
    var $body = $("body");

    function setStickyClass(stickyClass) {
      $scrollBarLocation > 111
        ? $(".fontawesome-icon").addClass(stickyClass)
        : $(".fontawesome-icon").removeClass(stickyClass);
    }

    $body.hasClass("user-logged-in")
      ? setStickyClass("sticky--logged-in")
      : setStickyClass("sticky--logged-out");
  });

  var $menuLink = $(".menu--main li:not(:last-child) a");
  var $menuItems = $menuLink.parent("li").siblings();

  $menuLink.on({
    mouseenter: function () {
      $menuItems
        .find("a.is-active")
        .attr("data-active", "was-active-link")
        .removeClass("is-active");
    },
    mouseleave: function () {
      $menuItems
        .find("a[data-active='was-active-link']")
        .addClass("is-active")
        .attr("data-active", "is-active-link");
    },
  });

  var $menuIcon = $(".region.region-header .fontawesome-icon");
  var $menu = $(".menu-full");

  $menuIcon.on("click", function () {
    $menuIcon.toggleClass("clicked");
    $menu.toggleClass("show");
  });

  if ($("body").hasClass("user-logged-in")) {
    $(".menu-item.sign-out").removeClass("hide");
    $(".menu-item.sign-in").addClass("hide");
  } else {
    $(".menu-item.sign-in").removeClass("hide");
    $(".menu-item.sign-out").addClass("hide");
  }
})(jQuery, Drupal);

//# sourceMappingURL=script.js.map
