const { src, dest, watch, series, parallel } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const minifyCss = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
const sourceMaps = require("gulp-sourcemaps");
const minifyJs = require("gulp-uglify");
const envirnoments = require("gulp-environments");
const gulpStylelint = require("gulp-stylelint");
const clean = require("gulp-clean");
const development = envirnoments.development;
const production = envirnoments.production;

// jsTask
function jsTask() {
  return src("./src/js/*.js")
    .pipe(development(sourceMaps.init()))
    .pipe(production(minifyJs()))
    .pipe(development(sourceMaps.write("./")))
    .pipe(dest("./dist/js"));
}

// scssTask
function scssTask() {
  return src("./src/sass/**/*.scss")
    .pipe(
      development(
        gulpStylelint({
          failAfterError: false,
          reporters: [{ formatter: "string", console: true }],
        })
      )
    )
    .pipe(development(sourceMaps.init()))
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(autoprefixer({ cascade: false }))
    .pipe(production(minifyCss()))
    .pipe(development(sourceMaps.write("./")))
    .pipe(dest("./dist/css"));
}

//copyAssets
function copyAssets() {
  return src(["./src/assets/**/*"]).pipe(dest("./dist/assets/"));
}

// watchTask
function watchTasks() {
  watch("./src/sass/**/*.scss", series(scssTask));
  watch("./src/js/*.js", series(jsTask));
}

//cleanTask
function cleanTask() {
  return src(["./dist"], { allowEmpty: true }).pipe(clean());
}

exports.dev = series(cleanTask, copyAssets, scssTask, jsTask, watchTasks);

exports.prod = series(cleanTask, copyAssets, scssTask, jsTask);
